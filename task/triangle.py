def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a<=0 or b<=0 or c<=0:
        return False

    sum=a+b+c
    big_side=max_of_three(a,b,c)

    if(2*big_side>=sum):
        return False
    
    return True

def max_of_three(a, b, c):

    if a>=b and a>=c:
        return a
    
    if b>=a and b>=c:
        return b
    
    return c